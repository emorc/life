/**
 * Created by emmanuel on 06/11/14.
 */

'use strict';

var life = {
    canvas: null,
    height: 10,
    width: 20,
    cellSize: 20,
    cellArray: [],
    board: [],
    tempBoard: [],
    speed: 1,
    interval: null,

    init: function(board, args){
        var $this = this;
        this.canvas = $("#board");
        this.board = board;
        this.height = board.length;
        this.width = board[0].length;

        if(args) {
            this.height = args.height || this.height;
            this.width = args.width || this.width;
            this.cellSize = args.cellSize || this.cellSize;
        }

        this.canvas.css({
            width: this.width * this.cellSize,
            height: this.height * this.cellSize
        });

        $("#next").on("click", function (e) {
            e.preventDefault();
            $this.step();
        });

        $("#play").on("click", function (e) {
            e.preventDefault();
            $this.play();
        });

        $("#pause").on("click", function (e) {
            e.preventDefault();
            $this.pause();
        });



        this.createBoard();

        for(var i = 0; i < this.height; i++){
            var row = [];
            for(var j = 0; j < this.width; j++ ) {
                row.push(0);
            }
            this.tempBoard.push(row);
        }

    },

    updateBoard: function () {
        for(var i = 0; i < this.height; i++){
            for(var j = 0; j < this.width; j++ ) {
                this.board[i][j] = this.tempBoard[i][j];
            }
        }
    },

    createBoard: function() {
        var $this = this;

        for(var h = 0; h < $this.height; h++) {
            var row = [];
            for (var w = 0; w < $this.width; w++) {

                var stateClass = ($this.board[h][w] ? 'live' : 'dead' );

                var cell =  $("<div />", {
                }).addClass("cell").css({
                    height: $this.cellSize,
                    width: $this.cellSize,
                    left: w * $this.cellSize,
                    top: h * $this.cellSize
                }).addClass(stateClass);

                row.push(cell);
                $this.canvas.append(cell);
            }
            $this.cellArray.push(row);
        }
    },

    countLiveNeighbours : function(x, y) {
        var $this = this;
        var liveNeighbours = 0;
        for(var i = x - 1; i <= x + 1; i++ ) {

            for(var j = y -1; j <= y + 1; j++ ) {
                if( (x == i && y == j ) || (i  < 0 || j < 0) || ( j >= $this.width || i >= $this.height ) ) {
                    continue;
                }
                if($this.board[i][j] == 1) {
                    liveNeighbours++;
                }
            }
        }
        return liveNeighbours;
    },

    play: function () {
        var $this = this;
        $this.speed = 10000 / parseInt($("#speed").val());
        this.interval = setInterval(function () {
            $this.step();
        }, $this.speed);

    },

    pause: function () {
       clearInterval(this.interval);
    },

    step: function ( ) {

        var $this = this;

        for(var h = 0; h < this.height; h++) {
            for (var w = 0; w < this.width; w++) {
                var currentCell = this.board[h][w];
                var liveNeighbours = this.countLiveNeighbours(h, w);
                var state = 0;

                if(currentCell == 1) {
                    if( liveNeighbours < 2 ) {
                        state = 0;
                    } else if (liveNeighbours > 3) {
                        state = 0;
                    } else {
                        state = 1;
                    }
                }
                else {
                    if(liveNeighbours == 3) {
                        state = 1;
                    }
                }

                this.changeState(h, w, state);
            }
        }


        $this.updateBoard();
    },

    changeState: function (x, y, state) {



        var currentCell = this.cellArray[x][y];
        currentCell.removeClass("live").removeClass("dead");
        var stateClass = state == 1 ? "live" : "dead";

        currentCell.addClass(stateClass);
        this.tempBoard[x][y] = state;


    }


};

$(document).ready(function(){

    var board = [
        [0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,1],
        [0,1,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1],
        [0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,1,0,0],
        [0,0,1,0,1,0,0,0,0,1,0,1,0,0,0,0,1,0,0,0],
        [0,1,0,0,1,0,0,0,1,0,0,1,0,1,0,0,0,1,0,0],
        [0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,1,0],
        [0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,1,0,0],
        [0,0,0,0,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0,1],
        [0,1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1,0,0,0],
        [0,0,0,0,0,1,1,0,0,0,0,1,0,1,0,0,0,1,0,0],
        [0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,1],
        [0,1,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1],
        [0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,1,0,0],
        [0,0,1,0,1,0,0,0,0,1,0,1,0,0,0,0,1,0,0,0],
        [0,1,0,0,1,0,0,0,1,0,0,1,0,1,0,0,0,1,0,0],
        [0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,1,0],
        [0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,1,0,0],
        [0,0,0,0,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0,1],
        [0,1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1,0,0,0],
        [0,0,0,0,0,1,1,0,0,0,0,1,0,1,0,0,0,1,0,0]
    ];

    life.init(board);
});